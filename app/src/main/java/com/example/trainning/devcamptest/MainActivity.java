package com.example.trainning.devcamptest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import android.os.AsyncTask;
import android.os.StrictMode;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity {

    String ip = "192.168.0.103";
    int port = 8000;
    MyClientTask myClientTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Log.d("LOG", "onCreate");
    }

    protected void onClickConnect(View view){
        Log.d("LOG socket connection", "Before start socket connection");
        myClientTask = new MyClientTask(ip, port);
        myClientTask.execute();
    }

    protected void onclickSend(View view){
        myClientTask.sendData("door");
        String result = myClientTask.fromServer();
        Log.d("LOG socket connection", "Send data to server: success");
        Toast.makeText(getApplicationContext(), "send data.\nget: "+result, Toast.LENGTH_SHORT);
    }

    protected void onclickClose(View view){
        myClientTask.closeSocket();
        Log.d("LOG socket connection", "Socket Closed");
    }



    public class MyClientTask extends AsyncTask<Void, Void, Void> {

        String dstAddress;
        int dstPort;
        String response = "";

        String sentence, fromSev;
        PrintWriter outToServer;
        BufferedReader inFromServer;
        Socket socket = null;


        MyClientTask(String addr, int port){
            dstAddress = addr;
            dstPort = port;
            Log.d("LOG socket connection", "After create MyClientTask object: success");
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            Log.d("LOG socket connection", "Before new Try");

            try {
                socket = new Socket(dstAddress, dstPort);
                Log.d("LOG socket connection", "After create socket connection: success");

                outToServer = new PrintWriter(socket.getOutputStream(),true);
                Log.d("LOG socket connection", "After create outToServer: success");

                inFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                Log.d("LOG socket connection", "After create inFromServer object: success");

                InputStream inputStream = socket.getInputStream();
                Log.d("LOG socket connection", "After create InputSteam object: success");

                sendData("java");
                fromServer();


            } catch (UnknownHostException e) {
                e.printStackTrace();
                response = "UnknownHostException: " + e.toString();
            } catch (IOException e) {
                e.printStackTrace();
                response = "IOException: " + e.toString();
            }
            Log.d("LOG socket connection", "Socket response: "+response);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.d("LOG socket connection", "OnPostExe: success");
            super.onPostExecute(result);
        }



        protected void closeSocket(){
            if(socket != null){
                try {
                    // code here
                    sendData("e");
                    Log.d("LOG socket connection", "Socket close by client");
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        protected void sendData(String data){
            //code here
            Log.d("LOG socket connection", "Sending: "+data+" to server.");
            sentence = data;
            outToServer.write(sentence);
            outToServer.flush();
        }

        public String fromServer(){
            Log.d("LOG socket connection", "fromServer method");
            try {
                fromSev =  inFromServer.readLine();
                Log.d("LOG socket connection", "Receive data: "+fromSev+" from server.");
                return fromSev;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "Nothings";
        }

    }

}
