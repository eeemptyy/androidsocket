//package com.example.trainning.devcamptest;
//
///**
// * Created by trainning on 7/18/2016.
// */
//
//import android.util.Log;
//import android.widget.Toast;
//
//import java.net.*;
//import java.io.*;
//
//
//public class TCPClient {
//
//    String sentence, fromSev;
//    PrintWriter outToServer;
//    BufferedReader inFromServer;
//    Socket clientSocket;
//
//    public TCPClient(String ip, int port){
//        try {
//            Log.d("LOG", "BF socket");
//            clientSocket = new Socket(ip, port);
//            Log.d("LOG", "AF socket");
//            outToServer = new PrintWriter(clientSocket.getOutputStream(),true);
//            Log.d("LOG", "BF inFromServer");
//            inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
//            Log.d("LOG", "AF inFromServer");
//        } catch (IOException e) {
//            e.printStackTrace();
//            Log.d("LOG", e.toString());
//        }
//    }
//    public void sendText(String inFrom){
//        sentence = inFrom;
//        outToServer.println(sentence);
//    }
//    public String fromServer(){
//        Log.d("LOG", "In from server");
//        try {
//            Log.d("LOG", "bf Readline");
//            fromSev =  inFromServer.readLine();
//            Log.d("LOG", "bf return");
//            return fromSev;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return "Nothings";
//    }
//    public void closeSocket() throws Exception {
//        clientSocket.close();
//    }
//
//}
